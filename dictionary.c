#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	// Allocate memory for an array of strings.
	int arrLength = 5;
	char **arr = malloc(arrLength * sizeof(char *));

	// Read the dictionary line by line.
	int numwords = 0;
	char line[50];
	while (fgets(line,50,in) != NULL)
	{
		//strip newline char
		char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        
        //dynamically allocate array size
		if (numwords == arrLength)
		{
			arrLength += 5;
			printf("arrLength now: %d\n", arrLength);
			arr = realloc(arr, arrLength * sizeof(char *));
		}
		
		// Copy each line into the array of strings (strcpy).
		char *word = malloc(20 * sizeof(char));
		strcpy(word, line);
		arr[numwords] = word;
		//printf("%d%s\n", numwords, word);
		
		numwords++;
	}
		
	// The size should be the number of entries in the array.
	*size = numwords;
	
	// Return pointer to the array of strings.
	return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}